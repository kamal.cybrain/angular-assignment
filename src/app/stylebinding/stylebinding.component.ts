import { Component } from '@angular/core';

@Component({
  selector: 'app-stylebinding',
  templateUrl: './stylebinding.component.html' ,
  styles: [
    `
    .highlighted {
      background-color: yellow;
      color: black;
    }
    `
  ]
})

export class MyComponent {
  isHighlighted: boolean = true;
}