import { Component } from '@angular/core';

@Component({
  selector: 'app-databinding',
  template: `<h3>Interpolation</h3>
  <li>first_name: {{ first_name }}</li>  
  <li>sex: {{ sex }}</li>

  <h3>Property Binding</h3>
  <input type="text" [value]="userName">
  
  <h3>Event Binding</h3>
  <button (click)="handleCLICK()">ClickOnce</button>
  <h3>{{eventbinding}}</h3><br><br>
  
  ` ,
  styleUrls: ['./databinding.component.scss']
})
export class DatabindingComponent {
  first_name="rajendra"
  sex="male"
  userName:string = "kamal"; 

  eventbinding!: string;
  handleCLICK(){

    this.eventbinding = 'hello brother !how was your day'
  
   }
   

}
